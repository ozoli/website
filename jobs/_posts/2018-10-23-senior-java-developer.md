---
layout: job
title: Senior Java Developer
author: Ramon Wieleman
image: images/jobs/we-are-hiring.png
excerpt: You are a Senior Java developer that is driven by ambition. Always curious and wanting to learn all the time. During your day job as a consultant at one of our clients, but also at conferences, trainings and basically whenever you can. You are experienced in the Java ecosystem and see software development as a craft that is never perfected. You like to work with others who feel that way too. We founded Code Nomads for you!
---

# Senior Java Developer

You are a Senior Java developer that is driven by ambition. Always curious and wanting to learn all the time. During your day job as a consultant at one of our clients, but also at conferences, trainings and basically whenever you can. You are experienced in the Java ecosystem and see software development as a craft that is never perfected. You like to work with others who feel that way too. We founded Code Nomads for you!

## You have:
-	A bachelor or master’s degree in computer science / engineering
-	8+ years’ experience as a software developer in the Java ecosystem
-	Outstanding coding skills: it always works!
-	The ambition to become to grow and learn to become the best software developer
-	An independent mindset: you don’t wait for a briefing to get something done. You already know what needs to be done and come up with the best solution
-	A good knowledge of databases (SQL and No-SQL databases)
-	Worked in Agile/Scrum/Kanban teams
-	Experience with working with microservices
-	Basic knowledge of the latest front-end frameworks (Angular, React, Vue.js)
-	Good skills in English (writing and speaking)
-	A curiosity to discover the latest technologies
-	The willingness to share your experience with other developers at conferences, meetups and/or in blogs

## Why choose for Code Nomads?
-	We are small company where you’ll work with like-minded passionate software developers and you feel appreciated for the work you do.
-	We work on the and most innovative and challenging projects in the Netherlands, using the latest cutting-edge technologies
-	We’ll help you to learn how to speak on conferences and meetups and we’ll mentor you to become a true software development expert
-	We’re actively participating in various JUG’s and developer communities. We’re happy to introduce you to the most brilliant minds in the industry.
-	You’ll work with in an international environment with developers from all around the world. Currently: Australia, Russia, India, Turkey, Brazil, Poland, Brazil, Portugal

## What Code Nomads can offer you:
-	A salary that matches your skills, experiences and ambitions
-	Personal development plan to become an expert in new technologies
-	Profit sharing regulation
-	26 payed holidays
-	Educational budget of 3,000 euros per year
-	R&D Hackathon days with the team (multiple times of the year)
-	Work with the latest cutting-edge technologies at our clients
-	Dutch language courses for you and your family
-	Laptop of your own choice
-	Transportation budget
-	Pension scheme, based on your personal situation
-	If applicable: sponsorship for the visa application for you and your family

## About Code Nomads
Code Nomads is a Java consultancy company in Amsterdam of international developers, working on the most innovative and challenging projects. We are driven software engineers with a passion for technology. We bring together the best developers from all around the world. We love what we do and we practice what we preach.
We’re actively participating in meetup groups and developer communities in the Netherlands, because we believe in lifelong learning. To keep up with all rapid changes in software development, we challenge ourselves to learn something new every day.
We believe that you need to love your work and work on the projects you love. We share our passion for Java technology with each other, we work on the most challenging and innovative projects, we never stop learning and we go on many fun social activities together.

## Contact
Is the description above in line with your interests, ambitions and experience? Come and join the Code Nomads tribe now! You can contact Ramon Wieleman (founder of Code Nomads) if you’re interested in this position at [ramon@codenomads.nl](mailto:ramon@codenomads.nl) or call him at [+31-6-33899966](tel:+31-6-33899966)
