/*
    commands for testing article previews
 */

Cypress.Commands.add('testPreview', (title, description, url) => {
    cy.get('a').should('have.attr', 'href')
        .and('include', url);
    cy.get('h3[class="major"]').contains(title);
    cy.get('p[class="preview_excerpt"]').first().contains(description);
});

Cypress.Commands.add('testPreviewWithImage', (idx, title, description, url, imageUrl) => {
     cy.get('section[class="features"] article:nth-child(' + idx + ')').within(() => {
        cy.testPreview(title, description, url);
        if (imageUrl) {
            cy.get('img').should('have.attr', 'src').and('include', imageUrl);
        } else {
            cy.get('img').should('not.exist');
        }
    })
});