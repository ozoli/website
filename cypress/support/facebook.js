/*
    quick command for testing metadata for facebook preview cards
 */

Cypress.Commands.add('testFacebookCard', (title, description, image, type) => {
    cy.get('head meta[property="og:title"]')
        .should('have.attr', 'content', title);
    cy.get('head meta[property="og:description"]')
        .should('have.attr', 'content', description);
    cy.get('head meta[property="og:image"]')
        .should('have.attr', 'content').and('include', image);
    cy.get('head meta[property="og:image"]')
        .should('have.attr', 'content').and('include', 'http');
    cy.get('head meta[property="og:type"]')
        .should('have.attr', 'content', type);
});

Cypress.Commands.add('testFacebookCardWebsite', (title, description, image) => {
    cy.testFacebookCard(title, description, image, 'website');
});

Cypress.Commands.add('testFacebookCardArticle', (title, description, image, articleUrl) => {
    cy.testFacebookCard(title, description, image, 'article');
    cy.get('head meta[property="og:type"]')
        .should('have.attr', 'content', 'article');
    cy.get('head meta[property="og:url"]')
        .should('have.attr', 'content').and('include', articleUrl);
    cy.get('head meta[property="og:url"]')
        .should('have.attr', 'content').and('include', 'http');
});