/*
    quick command for testing metadata for twitter preview cards
 */

Cypress.Commands.add('testTwitterCard', (title, description, image) => {
    cy.get('head meta[name="twitter:title"]')
        .should('have.attr', 'content', title);
    cy.get('head meta[name="twitter:description"]')
        .should('have.attr', 'content', description);
    cy.get('head meta[name="twitter:image"]')
        .should('have.attr', 'content').and('include', image);
    cy.get('head meta[property="og:image"]')
        .should('have.attr', 'content').and('include', 'http');
    cy.get('head meta[name="twitter:site"]')
        .should('have.attr', 'content', '@codenomadsnl');
});