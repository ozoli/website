describe('The footer', function () {

    it("links to Twitter", function () {
        cy.visit('/');
        cy.get('section#footer ul > li').eq(0).find('a').as('twitter');

        cy.get('@twitter').should('have.attr', 'href')
            .should('eq', 'https://twitter.com/CodeNomadsNL');

        cy.get('@twitter').find('span.fa-twitter');
    })

    it("links to email", function () {
        cy.get('section#footer ul > li').eq(1).find('a').as('email');

        cy.get('@email').should('have.attr', 'href')
            .should('eq', 'mailto:info@codenomads.nl');

        cy.get('@email').find('span.fa-envelope');
    })

    it("links to Linkedin", function () {
        cy.get('section#footer ul > li').eq(2).find('a').as('linkedin');

        cy.get('@linkedin').should('have.attr', 'href')
            .should('eq', 'https://www.linkedin.com/company/code-nomads/');

        cy.get('@linkedin').find('span.fa-linkedin');
    })

    it("footer contains copyright info", function () {
        cy.get('section#footer').get('ul[class=copyright] > li').as('copyright');
        cy.get('@copyright').eq(0).should('have.text', '© Copyright ' + new Date().getFullYear() + ' - Code Nomads - Privacy statement Code Nomads');

        cy.get('@copyright').find('a').eq(0).should('have.text', 'Privacy statement Code Nomads')
        cy.get('@copyright').find('a')
        .should('have.attr', 'href')
        .then((href) => {
          cy.request(href) // make sure the privacy policy is a valid URL ie does not 404 or 500 etc
        })
    })
})

