describe('The home page', function () {

    it("page title is correct", function () {
        cy.visit('/');
        cy.title().should('eq', 'Code Nomads - World Class Software Developers');
    })

    it('head meta tags standard', () => {
        cy.visit('/');
        cy.get('head meta[name="description"]')
            .should('have.attr', 'content', 'Code Nomads - World Software Class Developers');
    })

    it('head meta tags for FaceBook Open Graph', () => {
        cy.visit('/');
        cy.testFacebookCardWebsite('Code Nomads', 'Code Nomads is a small company of 14 world class software developers focused on Java, Scala and fullstack software development. We are group of international developers, living in the Netherlands. We love what we do and we practice what we preach.', '/images/background.jpg');
    })

    it('head meta tags for Twitter Cards', () => {
        cy.visit('/');
        cy.testTwitterCard('Code Nomads', 'Code Nomads is a small company of 14 world class software developers focused on Java, Scala and fullstack software development. We are group of international developers, living in the Netherlands. We love what we do and we practice what we preach.', '/images/background.jpg');
    })

    it('contacts lead developer', () => {
        cy.visit('/');
        cy.get('div[id=lead]').within(() => {
            cy.get('h4').contains('Oliver Carr');
            cy.get('h5').contains('Lead Developer');
            cy.get('img').should('have.attr', 'src', '/images/people/oliver-carr.jpeg');
            cy.get('ul li:first').within(() => {
                cy.get('a').should('have.attr', 'href', 'mailto:oliver@codenomads.nl');
                cy.get('a').contains('oliver@codenomads.nl');
            })
            cy.get('ul li:last').within(() => {
                cy.get('a').should('have.attr', 'href', 'https://twitter.com/aussieOllie');
                cy.get('a').contains('@aussieOllie');
            })
        })
    })

    it('/about does not exist', () => {
        cy.visit('/about', {failOnStatusCode: false});
        cy.get('div[class="container"] h1').contains('404');
        cy.get('div[class="container"] p').first().contains('Page not found :(');
        cy.get('div[class="container"] p').last().contains('The requested page could not be found.');
    })
})
