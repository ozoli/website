describe('A Jobs preview page', function () {

    it('Jobs preview head meta tags standard', () => {
        cy.visit('/jobs.html');
        cy.get('head meta[name="description"]')
            .should('have.attr', 'content', 'Code Nomads - World Software Class Developers');
    })

    it("Job Senior Java Developer preview", function () {
        cy.fixture('jobs/senior_dev_20181023.json').then((senior_dev_20181023) => {
            cy.visit('/jobs.html');
            cy.testPreviewWithImage(1, senior_dev_20181023.title,
                senior_dev_20181023.description,
                senior_dev_20181023.url, senior_dev_20181023.preview_image);
            cy.get('section[class="features] article:nth-child(2)').should('not.exist');
        });
    })

    it("Job Senior Java Developer preview Details", function () {
        cy.fixture('jobs/senior_dev_20181023.json').then((senior_dev_20181023) => {
            cy.visit('/jobs.html');
            cy.get('section[class="features"] ul[class="actions"]').within(() => {
                cy.get('li a').should('have.attr', 'href').and('include',
                    senior_dev_20181023.url);
                cy.get('li a').contains('Details');
            })
        });
    })

    it('Jobs Overview head meta tags for FaceBook Open Graph', () => {
        cy.fixture('meta.json').then((main) => {
            cy.visit('/');
            cy.testFacebookCardWebsite(main.title, main.description, main.background_url);
        });
    })

    it('Jobs Overview head meta tags for Twitter Cards', () => {
        cy.fixture('meta.json').then((main) => {
            cy.visit('/');
            cy.testTwitterCard(main.title, main.description, main.background_url);
        });
    })
})