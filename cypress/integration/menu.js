describe('The menu', function () {
    it('loads sections on big screen', function () {
        cy.visit('/');
        cy.contains('About').click().url().should('include', '#about');

        cy.contains('Projects').click().url().should('include', '#customers');

        cy.contains('Jobs').click().url().should('include', '#vacancies');

        cy.contains('Contact').click().url().should('include', '#contact');

        cy.contains('Blog').click().url().should('include', 'blog');

    })



    it("loads sections on small screen", function () {
        cy.visit('/');
        cy.viewport('ipad-mini');

        cy.get('#hamburger').click();
        cy.get('#menu').contains('About').click();
        cy.url().should('include', '#about');

        cy.get('#hamburger').click();
        cy.get('#menu').contains('Projects').click().url().should('include', '#customers');
        //work around: otherwise is not loading the new url
        cy.reload();


        cy.get('#hamburger', {timeout: 6000}).click();
        cy.get('#menu').contains('Jobs').click().url().should('include', '#vacancies');
        cy.reload();

        cy.get('#hamburger').click();
        cy.get('#menu').contains('Contact').click().url().should('include', '#contact');
        cy.reload();

        cy.get('#hamburger').click();
        cy.get('#menu').contains('Blog').click().url().should('include', 'blog');
    })

})
