describe('A Jobs page', function () {

    it('Job page head meta tags standard', () => {
        cy.fixture('jobs/senior_dev_20181023.json').then((senior_dev_20181023) => {
            cy.visit(senior_dev_20181023.url);
            cy.get('head meta[name="description"]')
                .should('have.attr', 'content', 'Code Nomads - World Software Class Developers');
        });
    })

    it("Job Senior Java Developer page", function () {
        cy.fixture('jobs/senior_dev_20181023.json').then((senior_dev_20181023) => {
            cy.log(senior_dev_20181023.url);
            cy.visit(senior_dev_20181023.url);
            cy.get('header h2').contains(senior_dev_20181023.title);
            cy.get('header p').contains('Ramon Wieleman');
            cy.get('#senior-java-developer').contains(senior_dev_20181023.title);
            cy.get('section[class="wrapper"] div p:nth-child(2)').contains(senior_dev_20181023.description);
        });
    })

    it('Job page head meta tags for FaceBook Open Graph', () => {
        cy.fixture('jobs/senior_dev_20181023.json').then((senior_dev_20181023) => {
            cy.visit(senior_dev_20181023.url);
            cy.testFacebookCardArticle(senior_dev_20181023.title,
                senior_dev_20181023.description,
                senior_dev_20181023.image,
                senior_dev_20181023.url);
        });
    })

    it('Job page head meta tags for Twitter Cards', () => {
        cy.fixture('jobs/senior_dev_20181023.json').then((senior_dev_20181023) => {
            cy.visit(senior_dev_20181023.url);
            cy.testTwitterCard(senior_dev_20181023.title,
                senior_dev_20181023.description,
                senior_dev_20181023.image,
                senior_dev_20181023.url);
        });
    })

})
