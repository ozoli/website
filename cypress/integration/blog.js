describe('A blog page', function () {

    it("blog title is correct", function () {
        cy.fixture('blog/brian_goetz_20180423.json').then((goetz_20180423) => {
                cy.visit(goetz_20180423.url);
                cy.title().should('eq', goetz_20180423.title);
            }
        );
    })

    it('head meta tags for FaceBook Open Graph', () => {
        cy.fixture('blog/brian_goetz_20180423.json').then((goetz_20180423) => {
                cy.visit(goetz_20180423.url);
                cy.testFacebookCardArticle(goetz_20180423.title,
                    goetz_20180423.description,
                    goetz_20180423.image,
                    goetz_20180423.url);
            }
        );
    })

    it('head meta tags for Twitter Cards', () => {
        cy.fixture('blog/brian_goetz_20180423.json').then((goetz_20180423) => {
                cy.visit(goetz_20180423.url);
                cy.testTwitterCard(goetz_20180423.title,
                    goetz_20180423.description,
                    goetz_20180423.image);
            }
        );
    })

    it("Blog Brian Goetz preview", function () {
        cy.fixture('blog/brian_goetz_20180423.json').then((goetz_20180423) => {
                cy.visit('/blog.html');
                cy.testPreviewWithImage(3, goetz_20180423.title,
                    goetz_20180423.description,
                    goetz_20180423.url, goetz_20180423.image);
                cy.get('section[class="features] article:nth-child(2)').should('not.exist');
            }
        );
    })

    it("Blog Deternity 2018 preview Details", function () {
        cy.fixture('blog/devternity_20181201.json').then((goetz_20180423) => {
                cy.visit('/blog.html');
                cy.get('section[class="features"] ul[class="actions"]').within(() => {
                    cy.get('li a').should('have.attr', 'href').and('include',
                        goetz_20180423.url);
                    cy.get('li a').contains('Details');
                })
            }
        );
    })
})
