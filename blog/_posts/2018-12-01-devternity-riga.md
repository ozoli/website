---
layout: post
title: Devternity 2018 Riga
author: Oliver Carr, Vasco Veloso
image: images/posts/devternity-riga-2018.jpg
excerpt: CodeNomads, Oliver and Vasco visited Riga for Devternity 2018 this is what they learnt!
---

## Why Devternity?

[Devtermity](https://devternity.com/) is a one day conference held in Riga, Latvia every year since 2015 with the day before given to workshops. Touted as the top 3 international software development conference in Europe turning developers into engineering leaders with free beer afterwards. We couldn't resist! Below we describe what we learnt, discovered and enjoyed from each session we attended.

### 12 ways to make code suck less, by Venkat Subramaniam

Venkat is a great story teller and this talk was no different beginning with the quote: ["The first step in becoming a better programmer is to let go of the conviction that we can code it once and get it right on the first write."](https://twitter.com/venkat_s/status/655246225617362944). Further emphasis that software development is a special form of non fiction writing Venkat recommends the book [On Writing Well by William Zissner](https://www.amazon.com/Writing-Well-Classic-Guide-Nonfiction/dp/0060891548).

The 12 Ways to make code suck less are:
- Schedule Time to Lower Technical Debt
- Favor High Cohesion
- Favor Loose Coupling
- Program with Intention
- [Avoid Primitive Obession](https://refactoring.guru/smells/primitive-obsession)
- Prefer Clear Code over Cleaver Code
- [Apply Zissner's Principle on Writing](https://lonetechnicalwriter.wordpress.com/2015/05/20/4-principles-of-writing/)
- Comment Why, Not What 
- Avoid Long Methods, Apply SLAP - [Single Level of Abstraction Principle](https://hackernoon.com/object-oriented-tricks-6-slap-your-functions-a13d25a7d994)
- Give Good Meaningful names
- Do Tactical Code Reviews
- Reduce State and State Mutation

Just like with the boyscout rule, I believe that the underlying message from Venkats' keynote is: "love your colleagues when writing code". This means that you actually care about the poor soul who will have to understand and maintain what you're writing. That's all about taking care of the future instead of fixing the past. There's lots of practical advice on the talk, but this is my takeaway.

{% youtube unT9lqlN26I %}

### Secure by design, by Eoin Woods

A talk packed with practical advice to build applications and systems from the ground up that are more secure.
In a nutshell, do think about practical security during the design phase. Don't try to add it afterwards, either it won't work or it will have as many holes as a Swiss cheese. Know the principle of least responsibility? That's just one of the design principles that were addressed. Great way to remember, or learn, them.

{% youtube fXb7qBLgTSA %}

### Does TDD really lead to good design?, by Sandro Marcuso

Not only this was the very first conference talk Vasco attended that was supported only by a whiteboard (that's right, no media) but it also got him thinking. Does good design really come out of TDD? The whole TDD cycle was discussed and dissected in front of our eyes. Personally, Vasco agrees, TDD can and will lead to good design... If the TDD cycle is allowed to go its full length and refactoring happens.

{% youtube KyFVA4Spcgg %}

### Making your tests rock in just 45 minutes, by Jakub Nabrdalik

This talk was full of practical advice about testing. For example, test what really matters. Test business functionality. Test modules as black boxes: don't rely on knowledge of their inner workings. Reduce distractions on tests, let the reader understand the test logic instead of its machinery: modularize them just like you would your production code, use helper libraries. Document your requirements on your tests.

{% youtube XhoxMG9KJXk %}

### The Way of a Jedi: How to Get Started, by Maxim Dorofeev

The talk introduces the concept of how we get things done and the moreethings we have to do the less we actually get done. Knowing does not correspond to doing. So what is needed? A tracking system free of judgement or guilt. So a Daily Checklist to encourage new behavour was shown with the following attributes:
- Indicator: useful for reflection
- Vaccine: Done once, result for a long time
- Practice: Result stays as long as you practice

These checklists are not to do lists. Research on how productive rested people are was shown with the following checklist:
- Indicator: Deep sleep duration
- Vaccine: Set blue light filter for screens, [flux](https://justgetflux.com/) desktops, [Twilight](https://play.google.com/store/apps/details?id=com.urbandroid.lux&hl=en) Android, iOS built in
- Practice: No screens in bed

More examples of checklists, good verbs bad verbs, classifying tasks and more was presented. Slides, the daily checklist, indictators and vaccines from Maxim Dorofeev is available [here](http://procrastinatology.com/2018/11/28/devternity-2018-riga-post-conference-post/).

{% youtube EN0kIzRB7Uk %}

### The Pillars of Domain-Driven Design, by Marco Heimeshoff

This talk uses the classic Eric Evans book [Domain-Driven Design: Tackling Complexity in the Heart of Software](https://www.amazon.com/Domain-Driven-Design-Tackling-Complexity-Software-ebook/dp/B00794TAUG) as a basis to work through a standard ecommerce example of online customer shopping for items to be delivered to them. The goal being; to "find the boundaries that enable your business to optimise it's core domains". The domains for this ecommerce example are Shopping, Warehouse, Payment and Delivery. So with understanding [Conway's Law](https://www.thoughtworks.com/insights/blog/demystifying-conways-law) we would create teams for example of these domains. The following code example was given:
```csharp
public class CustomerRepository
{
    IEnumerable<Customer> GetCustomerByYearOfBirth(DateTime yearOfBirth)
}
```

But the above code does not read as we would use it. For example, customers get customer by year of birth date time. So we would rewrite the above like:
```csharp
public class Customers
{
    IEnumerable<Customer> Born_in(Year year)
}
```

Now the above reads a lot better as we would use it. The difference in how these two definitions of the CustomerRepository would be written is:
```csharp
// Instead of
var repo = new CustomerRepository(/*db_connection */);
var c = repo.GetCustomerByYearOfBirth(new DateTime(1981,11,19,0,0,0)); 

// you write
var all_customers = new Customers(/*db_connection */);
var turning_50 = all_customers.Born_in(Year.of(1968))
```

The above uses a custom Value Type for Year which is shown in the video below.

{% youtube os9bxjR__94 %}

### The economics of software design, by J.B. Rainsberger

This talk was delivered without slides which at the end of the day was great. The key topic was balancing the effort required to refactor a code base. Will the company be taken over or run out of money soon? Then the effort to refactor will not pay off. Better fix the bugs found with tests and add tests for any new features required. If however there is time to refactor the value of the refactoring could be realised. Before the refactoring is completed delivery of value will decrease. 

Overall a very interesting description of a great way to describe the advantages of taking time to think, design, write tests and refactor in terms of economics and project management terminology. As someone would put it, "manager talk". Don't get the impression that this is not useful! Engineers need a perspective on the bigger picture! How else will you argue for or against a new system feature, a bug that must be fixed, a refactoring that will prevent a disaster?

{% youtube TQ9rng6YFeY %}

### Designing for performance, by Martin Thompson

Martin argues that Moore's Law is past its expiration date. Therefore all applications will have to be written with a real concern for their performance, in the sense that we can't trust that the hardware will be faster by the time the application is released. He argues that it will be just as fast. So please learn about hardware performance if you want your new applications to remain effective. Martin then goes on to describe topics such as concurrency and parallelism and their limits: you also can't just throw hardware at the problem. There will be a point where it will not get faster, but will instead slow down. Design patterns also may have an impact on performance.

{% youtube d50SxnI1D6Y %}

### Don't walk away from complexity, run!, by Venkat Subramaniam

An ending keynote reminding us all about the importance of reducing complexity on projects down to the minimum. Problems are complex, but there is no point in adding more complexity on top of them. So we should try to detect accidental complexity and remove it. This is not onlyabout design, it's also about the practical points of implementation: think about excessive dependencies, choosing technologies based on emotion, getting limited by frameworks, etc. This is definitely advice to return to every now and then.

{% youtube d6CYdTOLW8I %}

## Conclusion

Why is Devternity worth attending? Or at least the videos worth watching?
- Technology agnostic: avice applicable to almost every project!
- Time: one day, all speakers are excellent and in a small venue so meeting speakers and other attendees is actually possible.
- Location: Riga, very interesting city historically and food wise. Not too expensive either! 

So to answer our question yes Devterntiy rocks! Will you be at [Devternity 2019](https://devternity.com/)? We will both try to be there! The complete list of videos is available [here](https://www.youtube.com/playlist?list=PLZQDTUIhS27BzTufZCCYxZpvpPtSrQoe1) from the [Devternity YouTube channel](https://www.youtube.com/channel/UCvzMZyJZZ3XYQwbvOACVYrQ).
