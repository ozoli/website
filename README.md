# website
Source code for the CodeNomads website

# Getting Started 

To install on a Linux based developer machine:
```bash
sudo apt-get install ruby-dev build-essential 
sudo gem install eventmachine
sudo gem install bundler
bundle install
```
To install on a OS X based developer machine:
```bash
sudo gem install eventmachine
sudo gem install bundler
sudo bundle install
```

# Development Flow

- Checkout master
- Create branch
- Push code when ready and pipeline will output website the branch is hosted on
- For master branch push will be automatically synced in production S3 bucket

Source code for the CodeNomads website

Solid State by HTML5 UP
html5up.net | @ajlkn

# End to End testing
The end to end tests are written in [Cypress](https://cypress.io).
Tests can be executed locally or against the website deployed to S3 bucket.
Before executing tests, npm dependencies required for running cypress need to be installed. To install dependencies, run the following command:
```bash
npm install
```
## Executing tests locally
To run the tests locally, you need to serve the website with Docker. For this, use docker-compose.yml file that is included in the project.
To start the docker container, run the following command from the project directory:
```bash
docker-compose up -d
```
This will build the website and make it available at http://localhost:4000/. Any changes you will make locally will also be immediately picked and built by Jekyll.

If you want to run website on a different port (for instance, on 8000), you can modify environment variable `PORT` and then run docker-compose like following:
```bash
PORT=8000 docker-compose up -d
```
To run tests execute the following command:

```bash
export WEBSITE_URL=http://localhost:4000/
CYPRESS_baseUrl=$WEBSITE_URL $(npm bin)/cypress open
```
Be aware that if you changed the port on which the website is hosted, you need to also change it in the `WEBSITE_URL` variable in the previous command in order for cypress to properly run. 

To stop docker service, run the following command:
```bash
docker-compose stop
```

## Executing tests against the website deployed to S3 bucket

To run the tests against the S3 bucket, run the following commands where WEBSITE_URL is the URL of the S3 bucket where the site is hosted.

```bash
export WEBSITE_URL=http://website-internal-codenomads-feature-update-fonts-images-148.s3-website.eu-central-1.amazonaws.com/
CYPRESS_baseUrl=$WEBSITE_URL $(npm bin)/cypress open
```

## Important Notes Before Pushing Commits

Before pushing your changes to repository, please optimize images using a script. Please follow the steps:

1. Use 'Google Chrome' as browser.
2. Install 'Google Lighthouse' extension to 'Google Chrome' to investigate performance of the web application.
3. Using 'Google Lighthouse' extension, generate a performance report.
4. If there is a section called 'Properly size images' in the performance issues section, you need to optimize some of the images.
Otherwise, you don't need to proceed further.
5. In order to generate a report file, click on the 'Share' button on top of the report page and 'Save as JSON'. Or click on the download button in the menu bar of the Audits tab in the Chrome Developer Tools.
6. Move the report JSON file to `/scripts` directory.
7. The following script uses `Jimp` to resize images from `NPM`. Therefore, if you haven't run `npm install` to install `Jimp` run:
```bash
npm install
```
8. Run the `image-resizer.js` script with the following command: 
```bash
cd scripts/
node image-resizer.js <<json_report_file_name>>
```
9. Wait for the script to be completed.
10. After optimizing the images, please proceed to step #3.

NOTE:**
* The problematic image files in the report must be inside the directory `/images` or its subdirectories.

## Modifying Compression of HTMLs and assets

#### HTML Compression

To enable the compression of HTML pages, simply add the following code block on top of the `_layouts/default.html`:

```ruby
---
layout: compress
---
```

To change the options of compression, modify the section `compress_html` in `_config.yml` configuration file.

To disable HTML compression, simply remove the layout in the root layout file `_layouts/default.html`.

#### Image Compression

To enable image compression, have a section in the `_config.yml` file with the following sample configuration:

```yaml
image_optim:
  archive_dir: "images/originals"
  cache_file: "images/_image_cache.yml"
  image_glob: "images/**/*.{gif,png,jpg,jpeg}"
```

To change the options of compression, modify the section `image_optim` in `_config.yml` configuration file.

To disable image compression, simply remove the config section `image_optim` in `_config.yml` configuration file.

Credits:
        Theme:  
                Solid State by HTML5 UP html5up.net | @ajlkn

	Icons:
		Font Awesome (fontawesome.io)
		
	Plugins:
	    Jekyll - Easy Youtube Embed (https://github.com/pibby)
	    Compress HTML in Jekyll (http://jch.penibelst.de/)
	    image_optim-jekyll-plugin (https://github.com/chrisanthropic/image_optim-jekyll-plugin)

	Other:
		jQuery (jquery.com)
		Scrollex (github.com/ajlkn/jquery.scrollex)
		Responsive Tools (github.com/ajlkn/responsive-tools)
		Jimp (https://www.npmjs.com/package/jimp)

