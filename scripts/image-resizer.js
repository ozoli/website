var Jimp = require('jimp');
var fs = require('fs');

var jsonFileName = process.argv[2];

// Parsing report file to JSON.
var reportJson = JSON.parse(fs.readFileSync(jsonFileName, 'utf8'));

console.log('Starting to process file: \'' + jsonFileName + '\'');

// JSON token for the images to be optimized.
var toBeOptimizedImagesJson = reportJson['audits']['uses-responsive-images']['details']['items'];

// Looping for the images to be optimized.
for (var i = 0; i < toBeOptimizedImagesJson.length; i++) {
	console.log((i + 1) + '. ------------');

	var url = toBeOptimizedImagesJson[i].url;
	var resizeRatio = (100 - toBeOptimizedImagesJson[i].wastedPercent) / 100;

	var filePath = './..' + decodeURIComponent(url.substr(url.lastIndexOf('/images')));

	console.log(' > ' + 'Resizing ratio: ' + Math.round(resizeRatio * 10000) / 10000 + '% for file: \'' + filePath + '\'');

	var backupFilePath = backupFile(filePath);

	resizeImage(backupFilePath, filePath);
}

function backupFile(filePath) {
	// Renaming the original file to keep if something goes wrong or for backup.
	var backupFilePath = filePath.substr(0, filePath.lastIndexOf('.')) + "_backup" + filePath.substr(filePath.lastIndexOf('.'));
	fs.renameSync(filePath, backupFilePath);

	console.log(' > ' + 'Renamed original file from \'' + filePath + '\' -> \'' + backupFilePath + '\'');

	return backupFilePath;
}

function resizeImage(oldFilePath, newFilePath) {
	// Opening the file to resize
	Jimp.read(oldFilePath)
		.then(image => {
			// Scaling the image size.
			return image.scale(resizeRatio)
				.quality(60) // setting JPEG quality to 60% 
				.write(newFilePath); // saving the resized file.
		})
		.catch(err => {
			console.error(err);
		});

	console.log(' > ' + 'Resized image: \'' + filePath + '\'');
}
