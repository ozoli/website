#!/usr/bin/env bash

URL=$1
echo "URL for Jekyll Config YML: ${URL}"
sed -i -e "s~URL~${URL}~g" _config.yml
echo "Jekyll Config YML"
cat _config.yml
