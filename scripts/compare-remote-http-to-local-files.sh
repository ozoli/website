#!/usr/bin/env bash

FILE_LIST=$1
PATH_PREFIX=$2
URL=$3

CHANGED_FILE_TEST=$(diff -rq public public_master | grep -qv "Only"; echo $?)
if [ "$CHANGED_FILE_TEST" == 1 ]; then
  echo "No files changed"
  exit 0
fi

echo "Path prefix: ${PATH_PREFIX}"
echo "URL: ${URL}"

diff_failed=false

while read path; do
  testURL="${URL}${path}"
  testFile="${PATH_PREFIX}${path}"
  diff <(curl -s "${testURL}") "${testFile}"
  if [ $? == 1 ] && [ "$diff_failed" == false ];
  then
    echo "Diff failed"
    diff_failed=true
  fi
done < ${FILE_LIST}

echo "End Diff failed: ${diff_failed}"
if [ "$diff_failed" == true ];
then
  echo "Return failure exit code"
  exit 1
fi
