const headerHeight = document.querySelector('#header').offsetHeight;

const collapseWidth = 768;

const findCurrentSection = (reverse) => {
    const sections = document.querySelectorAll('section[id=wrapper] > section[id]');
    const currentY = window.scrollY;
    const windowHeight = window.innerHeight;
    let sectionArr = Array.from(sections);

    const windowUpper = window.scrollY;
    const windowLower = windowUpper + window.innerHeight;

    if (currentY === 0) {
	return null;
    }

    // if we're at the bottom of the page, take the last element
    if ((window.innerHeight + currentY) >= document.body.offsetHeight) {
        return sectionArr[sectionArr.length - 1];
    }
    
    if (reverse) { // scrolling up
	sectionArr.reverse();
    }
    const visibleSections = sectionArr
	  .filter(section => {
	      const sectionY = section.getBoundingClientRect().y;
	      return sectionY >= 0 && sectionY <= windowHeight;
	  });

    // take the first match
    return visibleSections[0];
}

const setSelectedMenuItem = (selectedItem) => {
    if (typeof selectedItem === 'undefined' || selectedItem == null) {
	return;
    }
    const itemId = selectedItem.id;
    const menuItems = document.querySelectorAll('header nav a');
    const selectedClass = 'selected';
    Array.from(menuItems)
	.forEach(item => {
	    const href = item.href;
	    if (href.endsWith(`#${itemId}`)) {
		if (!item.classList.contains(selectedClass)) {
		    item.classList.add(selectedClass);
		}
	    }
	    else {
		if (item.classList.contains(selectedClass)) {
		    item.classList.remove(selectedClass);
		}
	    }
	});
}

window.addEventListener('scroll', (evt) => {
    const currentY = window.scrollY;
    const header = document.querySelector('#header');
    const opaqueClass = 'opaque';
    if (currentY > headerHeight) {
	if (!header.classList.contains(opaqueClass)) {
	    header.classList.add(opaqueClass);
	}
    } else {
	if (header.classList.contains(opaqueClass)) {
	    header.classList.remove(opaqueClass);
	}
    }
    
    const currentSection = findCurrentSection();
    // remove selected class
    setSelectedMenuItem(currentSection);
});
