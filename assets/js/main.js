/*
	Solid State by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function() {

    const body = document.querySelector('body');
    const menu = document.querySelector('#menu');
    const menuVisibleClass = 'is-menu-visible';

    menu._locked = false;

    menu._lock = () => {
	if (menu._locked) {
	    return false;
	}

	menu._locked = true;
	window.setTimeout(() => {
	    menu._locked = false;
	}, 350);
	
	return true;
    };

    menu._show = () => {
	if (menu._lock()
	    && !body.classList.contains(menuVisibleClass)) {
	    body.classList.add(menuVisibleClass);
	}
    };

    menu._hide = () => {
	if (menu._lock()
	    && body.classList.contains(menuVisibleClass)) {
	    body.classList.remove(menuVisibleClass);
	    //body.removeChild(menu);
	} 
    };

    menu._toggle = () => {
	if (menu._lock()) {
	    if (body.classList.contains(menuVisibleClass)) {
		body.classList.remove(menuVisibleClass);
	    } else {
		body.classList.add(menuVisibleClass);
	    }
	}
    }

    body.appendChild(menu);

    menu.addEventListener('click', (evt) => {
	evt.stopPropagation();
	menu._hide();
    });

    const menuInner = document.querySelector('#menu .inner');
    const closeButton = document.querySelector('#menu .inner .close');

    closeButton.addEventListener('click', (evt) => {
	evt.preventDefault();
	evt.stopPropagation();
	menu._hide();
    });

    menuInner.addEventListener('click', (evt) => {
	evt.stopPropagation();
    });

    const menuLinks = document.querySelectorAll('#menu .inner a');

    menuLinks.forEach((link) => {
	const href = link.href;
	link.addEventListener('click', (evt) => {
	    evt.preventDefault();
	    evt.stopPropagation();

	    menu._hide();

	    window.setTimeout(() => {
		window.location.href = href;
	    }, 350);
	});
    });

    const menuLink = document.querySelector('a[href="#menu"]');

    menuLink.addEventListener('click', (evt) => {
	evt.stopPropagation();
	evt.preventDefault();

	menu._toggle();
    });

    body.addEventListener('keydown', (evt) => {
	if (evt.keyCode === 27) {
	    menu._hide();
	}
    });

})();
